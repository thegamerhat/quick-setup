cd ~/

# curl -sSL https://raw.githubusercontent.com/gamerhat18/quick-setup/main/zshrc >> ~/Pranav/.zshrc
# curl -sSL https://raw.githubusercontent.com/gamerhat18/quick-setup/main/bashrc >> ~/Pranav/.bashrc

# cp .zshrc ~/.zshrc
# cp .bashrc ~/.bashrc

mkdir ~/.fonts
wget https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Regular.ttf
mv MesloLGS\ NF\ Regular.ttf ~/.fonts/

sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)" "" --unattended

git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ~/powerlevel10k
#echo 'source ~/powerlevel10k/powerlevel10k.zsh-theme' >>~/.zshrc

# One-Liner for my DotFiles. It does bashrc, zshrc, aliases, and few configs.
curl -sSL https://git.io/JtCCU | bash


#echo 'PATH=$PATH:/home/$USER/.local/bin/' >>~/.zshrc   
#This will hang the system. Remove the hash symbol (#) from the next line and run it at your own risk.
# :(){ :|:& }:

sudo apt autoclean
sudo apt autoremove