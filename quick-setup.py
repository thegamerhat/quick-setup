try:
    import gi
except Exception as e:
    if type(e) == ModuleNotFoundError:
        print('PyGTK Library not found!\nAttempting self install.')
        try:
            import os
            os.system('pip3 install pygobject elevate')
        except Exception as e:
            print('Failed self installtion. Trace:\n', e)
            exit(0)
        print('Entering Setup...')
        import gi
    else:
        raise ModuleNotFoundError

import os
import subprocess
import sys

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

# print(os.getcwd())

class QuickSetup(Gtk.Window):
    def __init__(self):
        Gtk.Window.__init__(self, title="Quick Setup")
        self.terminal_command = None
        self.set_border_width(100)
        Rabel = Gtk.Label("Quick Setup")
        self.add(Rabel)

win = QuickSetup()
win.connect("destroy", Gtk.main_quit)
win.show_all()
Gtk.main()
