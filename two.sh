
sudo apt update
sudo apt install aptitude

sudo aptitude install -y toilet \
	figlet \
	flameshot \
	cmatrix \
	tlp \
	htop \
	scrcpy \
	fdisk \
	terminator \
	nautilus \
	gnome-themes-extra \
	sl \
	kdeconnect \
	python3 \
	python3-pip \
	git \
	curl \
	software-properties-common \
	smartmontools \
  nvidia-settings #\
 # i965-va-driver-shaders \
 # intel-media-va-driver-non-free \
 # mesa-va-drivers \

cowsay "Hello Sakshi! I am setting up this PC for you! Relax and enjoy the process" | lolcat

pip3 install youtube-dl 
#pip3 install powerline-shell

cowsay " Installing Rust and Exa" | lolcat

# Install Rust compiler
curl https://sh.rustup.rs -sSf | sh

# Install Exa
wget -c https://github.com/ogham/exa/releases/download/v0.9.0/exa-linux-x86_64-0.9.0.zip
unzip exa-linux-x86_64-0.9.0.zip
sudo mv exa-linux-x86_64 /usr/local/bin/exa
rm exa-linux-x86_64-0.9.0.zip

cowsay " Now Installing Music Player " | lolcat
flatpak install -y flathub org.gnome.Lollypop

# Might be removed before in final release
cowsay " Now Installing Icon Pack " | lolcat
wget -qO- https://git.io/papirus-icon-theme-install | sh

echo ""
echo ""
echo "Almost everything is set up, but Hardware accelerated Video streaming."
echo "Check out this amazing guide to get it working."
echo ""
echo "https://www.linuxuprising.com/2018/08/how-to-enable-hardware-accelerated.html" | lolcat

#This will hang the system. Remove the hash symbol (#) from the next line and run it at your own risk.
# :(){ :|:& }:

sudo apt autoclean
sudo apt autoremove
