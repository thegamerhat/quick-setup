#This Script:
#iex ((New-Object System.Net.WebClient).DownloadString('https://git.io/Jt3LI'))

Add-Type -AssemblyName System.Windows.Forms
[System.Windows.Forms.Application]::EnableVisualStyles()

$ErrorActionPreference = 'SilentlyContinue'
$wshell = New-Object -ComObject Wscript.Shell
$Button = [System.Windows.MessageBoxButton]::YesNoCancel
$ErrorIco = [System.Windows.MessageBoxImage]::Error
$Ask = 'Do you want to run this as an Administrator?
        Select "Yes" to Run as an Administrator
        Select "No" to not run this as an Administrator
        
        Select "Cancel" to stop the script.'

If (!([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole]'Administrator')) {
    $Prompt = [System.Windows.MessageBox]::Show($Ask, "Run as an Administrator or not?", $Button, $ErrorIco) 
    Switch ($Prompt) {
        #This will debloat Windows 10
        Yes {
            Write-Host "You didn't run this script as an Administrator. This script will self elevate to run as an Administrator and continue."
            Start-Process PowerShell.exe -ArgumentList ("-NoProfile -ExecutionPolicy Bypass -File `"{0}`"" -f $PSCommandPath) -Verb RunAs
            Exit
        }
        No {
            Break
        }
    }
}


function installchoco { 
    Write-Host "Installing Chocolatey"
    Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
    choco install chocolatey-core.extension -y
    refreshenv
}

function installqbt {
    refreshenv
    Write-Host "Installing qBittorrent..."
    choco install qbittorrent -y
    Write-Host "qBittorent Installed."
}

function install7z {
    refreshenv
    Write-Host "Installing qBittorrent..."
    choco install 7zip.install -y
    Write-Host "qBittorent Installed."
}

function installTeamviewer {
    refreshenv 
    Write-Host "Installing Teamviewer..."
    choco install teamviewer -y
    Write-Host "Teamviewer Installed."
}

function runtv {
    Invoke-WebRequest https://raw.githubusercontent.com/gamerhat18/quick-setup/main/Windows/Initial.ps1 C:\Initial.ps1 #https://git.io/Jt3LI
    Invoke-WebRequest https://raw.githubusercontent.com/gamerhat18/win10script/master/essentialsInstall.ps1 C:\essentialsInstall.ps1 # MyWin10debloatFork
    & 'C:\Program Files (x86)\TeamViewer\TeamViewer.exe'
}

function downloadLinux {
    # Also downloads rufus
    Invoke-WebRequest https://github.com/pbatard/rufus/releases/download/v3.13/rufus-3.13.exe -OutFile C:\Rufus.exe 
    refreshenv

    # Put a magnet link for torrent of a Linux ISO
    #qbittorrent  --add-paused=false --save-path='C:\' --skip-dialog=true 'magnet:?xt=urn:btih:949a0f900077dd55a4f1310ef6f5d09571929b98&dn=linuxmint-20.1-mate-64bit.iso&tr=udp%3a%2f%2ftracker.opentrackr.org%3a1337%2fannounce'
}

function usefulsoft {
    refreshenv
    choco install vlc -y
    choco install sumatrapdf.install -y
    choco install googlechrome -y
    choco install everything -y
    choco install libreoffice-fresh -y

}

function ventoy {
    refreshenv
    Invoke-WebRequest https://github.com/ventoy/Ventoy/releases/download/v1.0.33/ventoy-1.0.33-windows.zip -OutFile C:\Ventoy.zip
    
}

installchoco

#install7z

#installqbt

installTeamviewer

#ventoy

runtv

#downloadLinux

# https://pop-iso.sfo2.cdn.digitaloceanspaces.com/20.04/amd64/nvidia/18/pop-os_20.04_amd64_nvidia_18.iso

# Chris Titus' Debloat Script command given below for quick copy-paste.
# iex ((New-Object System.Net.WebClient).DownloadString('https://git.io/JJ8R4'))
